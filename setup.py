#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages


def parse_requirements(requirements):
    with open(requirements) as f:
        return [l.strip('\n') for l in f if l.strip('\n') and not l.startswith('#')]


setup_requirements = []

test_requirements = ['pytest', 'pytest-runner', ]

setup(
    author="Volker Kempert",
    author_email='volker.kempert@almedso.de',
    python_requires=">=3.5",
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="Twisted REST api serial exploration",
    entry_points={
        'console_scripts': [
            'hils=esp.hils.cli:main',
            'hilsd=esp.hils.service:main',
        ],
    },
    install_requires=parse_requirements('./requirements.txt'),
    license="MIT license",
    long_description="EPS Embedded Production System (EPS) - Hardware In a Loop (HIL) Simulator",
    include_package_data=True,
    name='esp-hils',
    packages=find_packages(include=['esp', 'esp.*']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    version='0.1.0',
    zip_safe=False,
)
