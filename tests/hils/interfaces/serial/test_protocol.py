# -*- coding: utf-8 -*-

""" Tests for `command bus protocol` package. """

import pytest
from mock import Mock



from esp.hils.interfaces.serial.protocol import MessageRecorder


def test_get_messages():
    ctl = MessageRecorder()
    assert ctl.get_messages() == []
    ctl._record_message("foo")
    ctl._record_message("bar")
    assert ctl.get_messages() == ["foo", "bar"]
    assert ctl.get_messages() == []


def test_recording_messages_overflow():
    ctl = MessageRecorder()
    assert ctl.get_messages() == []
    for i in range(23):
        ctl._record_message(i)
    assert ctl.get_messages() == [
        3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
        13, 14, 15, 16, 17, 18, 19, 20, 21, 22 ]
