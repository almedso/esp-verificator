# -*- coding: utf-8 -*-
"""Main Entry Point for HIL Service

* Use single treaded eventloop (via twisted)
* provide REST based behavior injection API
* implements a simulation model of the real world
* implements interfaces to the subject under test

"""
import sys
import logging
from twisted.internet import reactor

from .rest.wsgi import prepare_serving_wsgi
from .config import configuration
from .common import get_service_info

from .interfaces.serial.manager import configure as serial_configure


def configure_loglevel(config=configuration):
    loglevel = configuration['general']['loglevel']
    if loglevel == 'debug':
        return logging.DEBUG
    elif loglevel == 'info':
        return logging.INFO
    elif loglevel == 'warning':
        return logging.WARNING
    elif loglevel == 'error':
        return logging.ERROR
    return logging.WARNING


logging.basicConfig(stream=sys.stdout, level=configure_loglevel())
logger = logging.getLogger(__name__)


def hello():
    logger.info('Reactor loop started')


def main():
    ( name, version ) = get_service_info()
    logger.info('Starting service "{0}@{1}"'.format(name, version))
    reactor.callWhenRunning(hello)
    reactor_args = {}
    reactor_args = prepare_serving_wsgi()
    serial_configure(configuration)
    reactor.run(**reactor_args)


if __name__ == "__main__":
    sys.exit(main())
