# -*- coding: utf-8 -*-
"""wsgi container to run restful api application.

Use twisted / wsgi protocol to implement the wsgi container.
"""
import logging
logger = logging.getLogger('rest-api')

from twisted.internet import reactor

from .api import app
from ..config import configuration


def prepare_serving_wsgi():
    """Instanciate a wsgi container into the twisted reactor"""
    reactor_args = {}

    def run_twisted_wsgi():
        from twisted.web.server import Site
        from twisted.web.wsgi import WSGIResource

        resource = WSGIResource(reactor, reactor.getThreadPool(), app)
        site = Site(resource)
        reactor.listenTCP(int(configuration['httpd']['port']), site, interface=configuration['httpd']['host'])

    if app.debug:
        # Disable twisted signal handlers in development only.
        reactor_args['installSignalHandlers'] = 0
        # Turn on auto reload.
        import werkzeug.serving
        run_twisted_wsgi = werkzeug.serving.run_with_reloader(run_twisted_wsgi)

    logger.info("Listen on host {0} port {1}".format(
        configuration['httpd']['host'],configuration['httpd']['port']
    ))
    run_twisted_wsgi()
    return reactor_args



