# -*- coding: utf-8 -*-
"""REST-ful API to the HIL Simulator.

Use flask_restful micro framework to implement api calls
in the pythonic way.

The flask (restful) application itself is running in a
wsgi container.
"""

from flask import Flask, request
from flask_restful import Resource, Api

import logging
logger = logging.getLogger(__name__)


app = Flask(__name__)
api = Api(app)

from .service import Shutdown, ModifyLogLevel, Info
from .serial import SerialLine


api.add_resource(Shutdown, '/service/stop')
api.add_resource(ModifyLogLevel, '/service/loglevel/<level>')
api.add_resource(Info, '/service/info')

api.add_resource(SerialLine, '/serial/<int:port_id>')
