# -*- coding: utf-8 -*-
"""REST-ful API to control the service"""

from flask import request
from flask_restful import Resource

import logging
logger = logging.getLogger(__name__)

from esp.hils.common import get_service_info

class ModifyLogLevel(Resource):
    def put(self, level):
        logger.info('Change Loglevel to {}'.format(level))
        if 'error' == level:
            logging.getLogger().setLevel(logging.ERROR)
        elif 'warning' == level:
            logging.getLogger().setLevel(logging.WARNING)
        elif 'info' == level:
            logging.getLogger().setLevel(logging.INFO)
        elif 'debug' == level:
            logging.getLogger().setLevel(logging.DEBUG)
        else:
            return { 'error': 'Unknown loglevel {0}'.format(level)}, 404


from twisted.internet import reactor
class Shutdown(Resource):
    def put(self):
        logger.info('Stop the service by API call')
        reactor.stop()


class Info(Resource):
    def get(self):
        (name, version)  = get_service_info()
        logger.info('Request server info for {0}@{1}'.format(name, version))
        return { 'service': name, 'version': version }
