# -*- coding: utf-8 -*-

"""Provide accessors for serial lines

The life time of the manager for serial lines is during the
entire application
"""

from twisted.internet.serialport import SerialPort
from twisted.internet import reactor
import logging

from .protocol import MessageRecorder

logger = logging.getLogger(__name__)


class Struct(dict):
    """Generic struct class that bundles module variables in one place"""
    def __init__(self, **kwargs):
        dict.__init__(self, kwargs)
        self.__dict__ = self


swo = Struct(
    identifier='swo',  # A symbolic name indicating the purpose
    device=None,  # The physical device
    messages=lambda: [],   # Accessor to the messages i.e. a function
    )
"""Single Wire Output (SWO) - Is a serial line as well """


def configure(config):
    """Configure SWO Connection

    .. note ::
        This does opens a serial port.

    @params config: Configuration object
    """
    if 'swo' in config:
        try:
            swo.device = config['swo']['device']
            baudrate = int(config['swo']['baudrate'])
            logger.info("SWO is configured at {0}".format(swo.device))
            protocol = MessageRecorder()
            s = SerialPort(protocol, swo.device, reactor, baudrate=baudrate)
            swo.messages = s.get_messages  # note this is binds s to the method
        except:
            logger.error("SWO configuration -> not configured")
            swo.device = None
            swo.messages = lambda: []
    else:
        swo.device = None
        swo.messages = lambda: []
        logger.info("SWO is not configured")

