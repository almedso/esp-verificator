
from twisted.protocols.basic import LineReceiver
import logging

logger = logging.getLogger(__name__)


class MessageRecorder(LineReceiver):

    def __init__(self):
        self.received_messages = []

    def get_messages(self):
        """ Clear the message buffer and return the captured messages

        :retruns: list of messages
        """
        msg = self.received_messages
        self.received_messages = []
        return msg

    def _record_message(self, message):
        """Record the (incoming) message in the message array

        Newer messages are at the end of the array.
        The array and limited in size to MAX_MSG.
        Older Messages get removed

        :param message: The message to record
        :type message: string
        """
        MAX_MSG = 19
        length = len(self.received_messages)
        if length > MAX_MSG:
            self.received_messages = self.received_messages[-MAX_MSG:]
        self.received_messages.append(message)

    def lineReceived(self, line):
        """ Record the received message
        Overwrite the processing - called from base class
        """
        try:
            data = line.rstrip()
            logger.debug("Received message: {0}".format(data))
            self._record_message(data)
        except ValueError:
            logging.error('Unable to parse data %s' % line)
            return
