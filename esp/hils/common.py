# -*- coding: utf-8 -*-
"""Common stuff that is needed at various situations

single place to prevent code duplication
"""

import pkg_resources

def get_service_info():
    NAME = 'esp-hils'  # the service name

    return (NAME, pkg_resources.get_distribution(NAME).version)
