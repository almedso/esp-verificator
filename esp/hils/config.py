# -*- coding: utf-8 -*-

import os
import configparser

_default_config = """
    [general]
    loglevel = debug

    # Restful API Endpoint
    [httpd]
    host =  0.0.0.0
    port = 8080

    # Interfaces to Subject under Test
    [serial.1]
    id = 1
    device = /dev/ttyUSB0
    baudrate = 115200

"""

configuration = configparser.ConfigParser()
# inject default configuration
configuration.read_string(_default_config)
# look for other places to get a configuration
configuration.read([
    '/etc/esp-hils/esp-hils.conf',
    os.path.expanduser('~/.esp-hils.conf')
    ], encoding='cp1250'
)
