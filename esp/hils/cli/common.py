"""Common stuff for click CLI"""

import sys
import click
import requests

from esp.hils.config import configuration


click.disable_unicode_literals_warning = True
port = configuration['httpd']['port']
host = configuration['httpd']['host']
if host == '0.0.0.0':
    host = 'localhost'

from os import environ
env_host =  environ.get('HILS_HOST')
if env_host:
    host = env_host
env_port =  environ.get('HILS_PORT')
if env_port:
    port = env_port

base_url = 'http://' + host + ':' + port


