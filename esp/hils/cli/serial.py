"""Console script for Serial Line Management"""
import sys
import click
import requests

from .common import base_url


@click.command()
def serials():
    """ List Serial Ports """
    r = requests.get(base_url + '/serial/0')
    if r.status_code != requests.codes.ok:
        print('Received StatusCode: {0}'.format(r.status_code), file=sys.stderr)
        sys.exit(1)
    print(r.text)
    return 0
