"""Console script for Service Management"""
import sys
import click
import requests

from .common import base_url


@click.group()
def service(args=None):
    """ Commands that impact the behavior of the hil service """
    return 0


@service.command()
@click.argument('level')
def loglevel(level):
    """ Set the loglevel of the service """
    r = requests.put(base_url + '/sys/loglevel/'+ level)
    if r.status_code != requests.codes.ok:
        print('Received StatusCode: {0}'.format(r.status_code), file=sys.stderr)
        sys.exit(1)
    return 0


@service.command()
def stop():
    """ Stop the service by API call """
    try:
        # this will never return - so an exception is expected
        r = requests.put(base_url + '/sys/stop')
    finally:
        return 0


@service.command()
def info():
    """Get the name and version of the service"""
    r = requests.get(base_url + '/sys/info')
    if r.status_code != requests.codes.ok:
        print('Received StatusCode: {0}'.format(r.status_code), file=sys.stderr)
        sys.exit(1)
    print(r.text)
    return 0

