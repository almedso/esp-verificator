"""Console script for CLI entry point"""
import click

from .service import service
from .serial import serials


click.disable_unicode_literals_warning = True


@click.group()
def main(args=None):
    """CLI to control Hardware in a Loop Simulator"""
    return 0

main.add_command(service)
main.add_command(serials)


##############################################################################

if __name__ == "__main__":
    sys.exit(main())
