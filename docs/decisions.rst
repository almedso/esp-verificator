================
Design Decisions
================


1. TDD for business logic

  * Enforces Correctness
  * Enforces predictability in future developments
  * Enforces extensibility

2. Single threaded

  * Enforces robustness
  * Enforces correctness
  * Recommended by twisted
  * No race conditions possible
  * Requires asynchronous api at process border to inject behavior

3. Asynchronous API to inject behavior

  * Enforces operability (i.e flexibility in deployment of bdd test suite, tm simulator and tscs simulator)
  * Available options: IP-socket, SOUP, RPC, XML, REST; decision for **REST** because it is stateless

    * REST support via **twisted**,**twisted wsgi protocol**, **flask** and **flask-restful**
    * Easy to read code, state of the art, lose coupling

4. CLI

  * CLI offers full flexibility to use the software in an interpretive way (efficient, low turn around)
  * Tests the REST API
  * Can be scripted in CI/CD scenarios
  * Use **click** (over argparse) - state of the art in terms of readability
