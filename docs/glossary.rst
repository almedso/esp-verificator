Glossary
========

.. |board| replace:: :term:`board`
.. |PCB| replace:: :term:`PCB`
.. |Sphinx| replace:: :term:`Sphinx`

.. glossary::

   board
      A synonym that refers to a single MCU and it's specific connected peripherals chips including those IC's that
      are fully controlled by the firmware.

      Most often this is done on a single |PCB| 

   PCB
      Printed Circuit Board

   Sphinx
      Sphinx is a tool that makes it easy to create intelligent and beautiful documentation. It was originally created for the Python documentation, and it has excellent facilities for the documentation of software projects in a range of languages.

   RST
      RST is an easy-to-read, what-you-see-is-what-you-get plain text markup syntax and parser system. It is useful for in-line program documentation (such as Python docstrings), for quickly creating simple web pages, and for standalone documents. |RST| is designed for extensibility for specific application domains. The |RST| parser is a component of Docutils.

