============
Requirements
============

Functional Requirements
=======================

.. req :: Support of BDD tests
   :id: REQ_001
   :tags: functional

   The HIL Simulator shall provide an API that allows instrumentation of a BDD testrunner.


.. req :: Support of Performance tests
   :id: REQ_002
   :tags: functional

   The HIL Simulator shall allow to put stress load on the SuT (Software).


.. req :: Support of Performance tests
   :id: REQ_003
   :tags: functional

   The HIL Simulator shall provide an allows to test stability and robustness of the SUT.


.. spec :: Spec for a requirement
   :links: REQ_001
   :status: done
   :tags: important; main_example
   :collapse: false

   We haven't set an **ID** here, so sphinx-needs
   will generated one for us.



Non-Functional Requirements
===========================

Constraints
...........

.. req :: Python3

.. req :: Twisted framework

.. req :: Run on Raspberry Pi


Software Quality Attributes
...........................

(lower numbers indicate higher priority)

.. req :: Usability: Correctness, Robustness and Functional Appropriateness
   :id: PRIO_01

   Ratio: Failures in test code do invalidate test results.


.. req :: Usability: Operability
   :id: PRIO_02

   Flexibility in deployment and configuration simplifies CI/CD and usage for developer. E.g.

   * Scenarios bdd test suite runs on CI job, while only tm simulator runs on HIL target hardware.
   * Stress tests can be instrumented on developers laptop, while load generation happens on HIL target
     hardware.


.. req :: Maintenance: Readability, Extensibility
   :id: PRIO_03

   * Use python style and convention
   * low coupling across modules
   * strong cohesion inside modules

