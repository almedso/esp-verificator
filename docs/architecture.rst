=====================
Software Architecture
=====================

Context Diagram
===============


.. uml ::

    @startuml

    interface "REST behavior injection" as rest
    interface "Single Wire Output" as swo
    interface "Generic XXX" as xxx

    [BDD Testrunner] -- rest
    [CLI] -- rest

    rest  - [HIL Simulator]
    [HIL Simulator] -- swo
    [HIL Simulator] -- xxx
    swo  -- [SUT]
    xxx  -- [SUT]

    @enduml


.. list-table:: Context View
   :widths: 15 5 50
   :header-rows: 1

   * - Element
     - Type
     - Description
   * - REST behavior injection
     - Interfaceestbed
     - http/REST API to inject the behavior of the HIL Simulator
   * - Generic XXX
     - Interface
     - Stereotype for any GPIO, I2C, SPI, UART, ...
       Multiple instances might exist from different type depends on SuT functionality
   * - Single Wire Output
     - Interface
     - Log Channel of the embedded device
   * - HIL Simulator
     - Component
     - The **HIL Simulator** interacts with the **SUT** (subject under test) via all
       exposed **SUT** Interfaces.
   * - Testrunner
     - Component
     - The runner is based on behave-bdd it reads feature files in gherkin language
       and instruments the HIL Simulator via to run the test.
   * - SUT
     - Component
     - The Unit to be blackbox tested.


Component View
==============

Abstract View
-------------

The abstract view introduces a architecture of an Hardware in a Loop Simulator (HILS)
to simulate an arbitrary real world environment as interfaced by a specific *SuT*.
The Interfaces between HILS and SuT can be anything like a tcp socket, serial port,
I2C bus, CAN bus, or simply a bunch of GPIO's.

This abstract view introduces a general approach of how to structure a HIL Simulator


.. uml ::

    @startuml
    interface "REST behavior injection" as rest_i
    interface "SUT - interface One" as sut_i

    [CLI / GUI] -- rest_i
    [BDD Suite Runner] -- rest_i

    component "hils" {
        rest_i -- ["REST API"]


        component "model" {
            [Real World Element 1]
            [Real World Element 2]
        }

        component "SuT Interface One" {
            [Protocol] -- sut_i
            [Manager]
            [Model]
        }

    }
    sut_i - [SuT]
    @enduml

    @enduml

.. list-table:: Abstract View
   :widths: 20 80
   :header-rows: 1

   * - Building Block
     - Description
   * - SuT Interface One
     - One type of interface between SuT and HILS. There could be multiple instances
       of the very same type. I.e. there is exactly one implementation for this type.
   * - Manager
     - The manager manages all interface instances, i.e. is responsible for instance instanciation
       and destruction, as well as for access to the interface instances and supervision/monitoring
   * - Protocol
     - The protocol implements the specific behavior of the interfaces for exactly one instance.
   * - Model
     - Modelling of the real world objects the SuT deals with
   * - REST API
     - Restful API that allows configuring the model and interfaces as well as it allows injecting
       behavior and querying for states.


Specific View
-------------

The specific view is more concrete about the future extension of the HIL Simulator.
It shall detail all the interfaces and simulation model.

.. todo :: Add later if it gets concrete



REST-ful API
------------


.. automodule :: esp.hils.rest.api
   :members:


CLI
---

There is a CLI python package that conveniently maps all rest call into about
cli (with self-explaining help) for easy instrumentation by developers/ tester.


Simulator Model
---------------

.. automodule :: esp.hils.interfaces
   :members:

Configuration
-------------

.. todo :: configuration

Runtime View
============

The HILS runs via twisted (single thread eventloop) with plugged in
in event handlers. Event handlers are implemented as protocols. They are

Restful API (via wsgi)
----------------------

The wsgi service is running under control of the twisted reactor.
The restful API is running asynchronously. the REST requests are
either querying or modifying the internal state of the simulator model.

