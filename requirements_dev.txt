pip==18.1
bumpversion==0.5.3
wheel==0.32.1
watchdog==0.9.0
coverage==4.5.1
Sphinx==1.8.1
sphinxcontrib-needs
sphinx_rtd_theme
sphinxcontrib-plantuml

pytest==3.8.2
pytest-runner==4.2
pylint==1.9.3 # pylint  2.0.0 and 2.0.1 are not working with setuptools: "python3 setup.py lint" crashes
pytest-mock
