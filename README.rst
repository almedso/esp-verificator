============================================
Embedded Software Production - HIL Simulator
============================================

Is a service running on e.g. a RaspberryPI, that is connected with
the SuT (the embedded firmware) and implements the environment
along I2C, GPIO, UART, SPI and other interfaces

The service serve a simple Restful API and can be instrumented
via cli, that is also part of this package.

Features
--------

* TODO

Usage
-----

* Install the as pypi package
* run exposed commands *hils* (the cli) or *hilsd* (the service)
* configuration at *~/.eps-hils.conf* or */etc/eps-hils/eps-hils.conf*

Building
--------

* pip3 install -r requirements_dev.txt
* python3 setup.py test
* python3 setup.py build_sphinx

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
